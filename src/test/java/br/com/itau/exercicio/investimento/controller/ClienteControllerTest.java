package br.com.itau.exercicio.investimento.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.exercicio.investimento.model.Cliente;
import br.com.itau.exercicio.investimento.service.ClienteService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {ClienteController.class})
public class ClienteControllerTest {
	
	@MockBean
	ClienteService clienteService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void deveObterListaDeClientes() throws Exception {
		//given
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Fiote");
		
		List<Cliente> clientes = Lists.newArrayList(cliente);
		
		//when
		when(clienteService.obterClientes()).thenReturn(clientes);
		
		//then
		mockMvc.perform(get("/cliente"))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("\"id\":1")))
		.andExpect(content().string(containsString("Fiote")))	
		;
	}
	
	@Test
	public void deveCriarUmCliente() throws JsonProcessingException, Exception {
		//given
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Crepúsculo");
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		//when
		mockMvc.perform(post("/cliente")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(cliente)))
				.andExpect(status().isCreated());
		
		//then
		verify(clienteService).criarCliente(any(Cliente.class));
		
	}
	
	@Test
	public void deveEditarUmCliente() throws JsonProcessingException, Exception {
		Long id = 1L;
		Long novoId = 10L;
		
		//given
		Cliente clienteDoBanco = new Cliente();
		clienteDoBanco.setId(id);
		clienteDoBanco.setNome("Fulano");
		
		Cliente clienteDoFront = new Cliente();
		clienteDoFront.setId(novoId);
		clienteDoFront.setNome("Beltrano");
		
		clienteDoBanco.setNome(clienteDoFront.getNome());

		when(clienteService.editarCliente(id, clienteDoBanco)).thenReturn(clienteDoBanco);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		//when
		mockMvc.perform(patch("/cliente/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(clienteDoBanco)))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("")));
		
	}
	
}