package br.com.itau.exercicio.investimento.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.exercicio.investimento.model.Produto;
import br.com.itau.exercicio.investimento.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ProdutoService.class })
public class ProdutoServiceTest {

	@Autowired
	ProdutoService produtoService;

	@MockBean
	ProdutoRepository produtoRepository;

	@Test
	public void criarUmProduto() {
		// DADO QUE
		Long id = 1L;
		Produto produto = criarProduto(id);
		produtoService.criarProduto(produto);
		verify(produtoRepository).save(produto);

	}

	@Test
	public void listarTodosOsProdutos() {
		// DADO QUE
		Long id = 1L;
		Produto produto = criarProduto(id);
		List<Produto> carteira = Lists.newArrayList(produto);

		Mockito.when(produtoService.obterProdutos()).thenReturn(carteira);
		List<Produto> resultado = Lists.newArrayList(produtoService.obterProdutos());
		assertNotNull(resultado.get(0));
	}

	@Test(expected = ResponseStatusException.class)
	public void lancarExcecoaQuandoNaoEncontrarAAplicacao() {
		// DADO QUE
		Long id = 1L;
		when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		produtoService.encontraOuDaErro(id);
	}

	@Test
	public void encontrarUmaAplicacao() {
		// DADO QUE
		Long id = 1L;
		Produto produto = criarProduto(id);

		when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));
		assertNotNull(produtoService.obterProduto(id));
	}

	@Test
	public void editarUmaAplicacao() {
		Long id = 1L;
		Long novoId = 100L;
		double novoValor = 200.0;
		Produto produtoBanco = criarProduto(id);
		Produto produtoFront = criarProduto(novoId);
		produtoFront.setRendimento(novoValor);

		when(produtoRepository.findById(id)).thenReturn(Optional.of(produtoBanco));
		when(produtoRepository.save(produtoBanco)).thenReturn(produtoBanco);

		Produto resultado = produtoService.editarProduto(id, produtoFront);
		assertNotNull(resultado);
		assertEquals(novoValor, resultado.getRendimento(), 0.5);
		assertNotEquals(novoId, resultado.getId());

	}

	@Test
	public void excluiAplicacao() {
		// DADO QUE
		Long id = 1L;
		Produto produto = criarProduto(id);
		when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));
		produtoService.apagarProduto(id);
		verify(produtoRepository).delete(produto);
	}

	private Produto criarProduto(Long id) {

		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("CDA");
		produto.setRendimento(10.0);
		return produto;
	}
}
