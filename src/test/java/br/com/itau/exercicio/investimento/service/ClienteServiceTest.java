package br.com.itau.exercicio.investimento.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.exercicio.investimento.model.Cliente;
import br.com.itau.exercicio.investimento.repository.ClienteRepository;




@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ClienteService.class})
public class ClienteServiceTest {
	
	@Autowired
	ClienteService clienteService;
	
	@MockBean
	ClienteRepository clienteRepository;
	

	@Test
	public void deveListarTodosOsClientes() {	
		long id = 1L;
		//given
		Cliente cliente = criarCliente(id);
		
		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);
		
		when(clienteRepository.findAll()).thenReturn(clientes);
		
		//when
		List<Cliente> resultado = Lists.newArrayList(clienteService.obterClientes());
		
		//check
		assertNotNull(resultado.get(0));
		assertEquals(cliente, resultado.get(0));
	}
	

	@Test
	public void deveCriarUmCliente() {
		long id = 1L;
		//given
		Cliente cliente = criarCliente(id);
		
		//when
		clienteService.criarCliente(cliente);
		
		//then
		verify(clienteRepository).save(cliente);
		
	}
	
	@Test
	public void deveEditarUmCliente() {
		//given
		long id = 1L;
		String novoNome = "Manézão";
		String novoCpf = "999.999.999-99";
		
		Cliente clienteDoBanco = criarCliente(id);
		Cliente clienteDoFront = criarCliente(id);
		clienteDoFront.setNome(novoNome);
		
		when(clienteRepository.findById(id)).thenReturn(Optional.of(clienteDoBanco));
		when(clienteRepository.save(clienteDoBanco)).thenReturn(clienteDoBanco);
		
		//when
		Cliente resultado = clienteService.editarCliente(id, clienteDoFront);
		
		//then
		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
		assertNotEquals(novoCpf, resultado.getCpf());
		verify(clienteRepository).save(clienteDoBanco);
	}
	
	
	
	
	
	
	
	private Cliente criarCliente(long id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNome("Zezinho");
		cliente.setCpf("301.032.984-02");
		
		return cliente;
	}
	
	
}
