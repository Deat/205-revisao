package br.com.itau.exercicio.investimento.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.exercicio.investimento.model.Aplicacao;
import br.com.itau.exercicio.investimento.model.Cliente;
import br.com.itau.exercicio.investimento.model.Produto;
import br.com.itau.exercicio.investimento.repository.AplicacaoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { AplicacaoService.class })
public class AplicacaoServiceTest {

	@Autowired
	AplicacaoService aplicacaoService;

	@MockBean
	AplicacaoRepository aplicacaoRepository;

	@Test
	public void criarUmaAplicacao() {

		// DADO QUE
		Long id = 1L;
		Aplicacao aplicacao = criarAplicacao(id);
		aplicacaoService.criarAplicacao(aplicacao);
		verify(aplicacaoRepository).save(aplicacao);

	}

	@Test
	public void listarTodasAsAplicacoes() {
		// DADO QUE
		Long id = 1L;
		Aplicacao aplicacao = criarAplicacao(id);

		List<Aplicacao> carteira = Lists.newArrayList(aplicacao);

		Mockito.when(aplicacaoService.obterAplicacaoes()).thenReturn(carteira);
		List<Aplicacao> resultado = Lists.newArrayList(aplicacaoService.obterAplicacaoes());
		assertNotNull(resultado.get(0));
	}

	@Test(expected = ResponseStatusException.class)
	public void lancarExcecoaQuandoNaoEncontrarAAplicacao() {
		// DADO QUE
		Long id = 1L;
		when(aplicacaoRepository.findById(id)).thenReturn(Optional.empty());
		aplicacaoService.encontraOuDaErro(id);
	}

	@Test
	public void encontrarUmaAplicacao() {
		// DADO QUE
		Long id = 1L;
		Aplicacao aplicacao = criarAplicacao(id);

		when(aplicacaoRepository.findById(id)).thenReturn(Optional.of(aplicacao));
		assertNotNull(aplicacaoService.obterAplicacao(id));
	}

	@Test
	public void editarUmaAplicacao() {
		Long id = 1L;
		Long novoId = 100L;
		double novoValor = 200.0;
		Aplicacao aplicacaoBanco = criarAplicacao(id);
		Aplicacao aplicacaoFront = criarAplicacao(novoId);
		aplicacaoFront.setValor(novoValor);

		when(aplicacaoRepository.findById(id)).thenReturn(Optional.of(aplicacaoBanco));
		when(aplicacaoRepository.save(aplicacaoBanco)).thenReturn(aplicacaoBanco);

		Aplicacao resultado = aplicacaoService.editarAplicacao(id, aplicacaoFront);
		assertNotNull(resultado);
		assertEquals(novoValor, resultado.getValor(), 0.5);
		assertNotEquals(novoId, resultado.getId());

	}

	@Test
	public void excluiAplicacao() {
		// DADO QUE
		Long id = 1L;
		Aplicacao aplicacao = criarAplicacao(id);
		when(aplicacaoRepository.findById(id)).thenReturn(Optional.of(aplicacao));
		aplicacaoService.apagarAplicacao(id);
		verify(aplicacaoRepository).delete(aplicacao);
	}

	private Aplicacao criarAplicacao(Long id) {

		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setId(id);
		aplicacao.setProduto(new Produto());
		aplicacao.setValor(100.00);
		aplicacao.setCliente(new Cliente());
		return aplicacao;
	}
}
