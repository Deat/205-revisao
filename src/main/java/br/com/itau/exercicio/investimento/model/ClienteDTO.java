package br.com.itau.exercicio.investimento.model;

public interface ClienteDTO {

	Long getId();
	
	String getNome();
	
	String getCpf();

}
