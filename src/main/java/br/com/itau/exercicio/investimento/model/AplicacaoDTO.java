package br.com.itau.exercicio.investimento.model;

public interface AplicacaoDTO {
	Long getId();

	Produto getProduto();

	double getValor();

	int getMeses();

	Cliente getCliente();
}
