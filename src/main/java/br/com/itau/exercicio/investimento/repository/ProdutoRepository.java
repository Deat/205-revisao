package br.com.itau.exercicio.investimento.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.exercicio.investimento.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{
}
