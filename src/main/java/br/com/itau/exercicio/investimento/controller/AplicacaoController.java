package br.com.itau.exercicio.investimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.exercicio.investimento.model.Aplicacao;
import br.com.itau.exercicio.investimento.service.AplicacaoService;

@RestController
public class AplicacaoController {

	@Autowired
	private AplicacaoService aplicacaoService;
	
	@GetMapping("/cliente/{id}/aplicacao/")
	public Iterable<Aplicacao> listarAplicacoes() {
		return aplicacaoService.obterAplicacaoes();
	}
	
	@GetMapping("/cliente/{id}/aplicacao")
	public Aplicacao listarAplicacao(@PathVariable Long id) {
		return aplicacaoService.obterAplicacao(id);
	}

	
	@PostMapping("/cliente/{id}/aplicacao/")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarAplicacao(@RequestBody Aplicacao aplicacao) {
		aplicacaoService.criarAplicacao(aplicacao);
	}
	
	@DeleteMapping("/aplicacao/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarAplicacao(@PathVariable Long id) {
		aplicacaoService.apagarAplicacao(id);
	}
	
	@PutMapping("/aplicacao/{id}")
	public Aplicacao editarFAplicacao(@PathVariable Long id, @RequestBody Aplicacao aplicacao) {
		return aplicacaoService.editarAplicacao(id, aplicacao);
	}
}
