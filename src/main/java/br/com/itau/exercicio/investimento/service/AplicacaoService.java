package br.com.itau.exercicio.investimento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.exercicio.investimento.model.Aplicacao;
import br.com.itau.exercicio.investimento.repository.AplicacaoRepository;

@Service
public class AplicacaoService {

	@Autowired
	private AplicacaoRepository aplicacaoRepository;

	public void criarAplicacao(Aplicacao aplicacao) {
		aplicacaoRepository.save(aplicacao);
	}
	
	public Iterable<Aplicacao> obterAplicacaoes() {
		return aplicacaoRepository.findAll();
	}
	
	public Aplicacao obterAplicacao(Long id) {
		return encontraOuDaErro(id);
	}

	public Aplicacao editarAplicacao(Long id, Aplicacao aplicacaoAtualizada) {
		Aplicacao aplicacao = encontraOuDaErro(id);
		
		aplicacao.setValor(aplicacaoAtualizada.getValor());
		
		return aplicacaoRepository.save(aplicacao);
	}
	
	public void apagarAplicacao(Long id) {
		Aplicacao aplicacao = encontraOuDaErro(id);
		aplicacaoRepository.delete(aplicacao);
	}

	public Aplicacao encontraOuDaErro(Long id) {
		Optional<Aplicacao> optional = aplicacaoRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}

		return optional.get();
	}
}
