package br.com.itau.exercicio.investimento.model;

public interface ProdutoDTO {
	Long getId();

	String getNome();

	String getRendimento();
}
