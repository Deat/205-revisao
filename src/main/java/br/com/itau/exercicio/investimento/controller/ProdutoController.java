package br.com.itau.exercicio.investimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.exercicio.investimento.model.Produto;
import br.com.itau.exercicio.investimento.service.ProdutoService;

@RestController
public class ProdutoController {

	@Autowired
	private ProdutoService produtoService;

	@PostMapping("/produto")
	@ResponseStatus(code = HttpStatus.OK)
	public Produto criarProduto(@RequestBody Produto produto) {
		return produtoService.criarProduto(produto);
	}

	@GetMapping("/produto")
	public Iterable<Produto> listarProdutos() {
		return produtoService.obterProdutos();
	}

	@GetMapping("/produto/{id}")
	public Produto listarProduto(@PathVariable Long id) {
		return produtoService.obterProduto(id);
	}

	@PutMapping("/produto/{id}")
	public Produto editarProduto(@PathVariable Long id, @RequestBody Produto Produto) {
		return produtoService.editarProduto(id, Produto);
	}

	@DeleteMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarProduto(@PathVariable Long id) {
		produtoService.apagarProduto(id);
	}

}
