package br.com.itau.exercicio.investimento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.exercicio.investimento.model.Cliente;
import br.com.itau.exercicio.investimento.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public Iterable<Cliente> obterClientes(){
		return clienteRepository.findAll();
	}
	
	public void criarCliente(Cliente cliente) {
		clienteRepository.save(cliente);
	}
	
	public Cliente editarCliente(Long id, Cliente clienteAtualizado) {
		Cliente cliente = encontraOuDaErro(id);
		
		cliente.setNome(clienteAtualizado.getNome());
		
		return clienteRepository.save(cliente);
	}
	
	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienteRepository.delete(cliente);
	}
	
	public Cliente encontraOuDaErro(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}
		
		return optional.get();
	}
}
