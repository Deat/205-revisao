package br.com.itau.exercicio.investimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.exercicio.investimento.model.Cliente;
import br.com.itau.exercicio.investimento.service.ClienteService;


@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public Iterable<Cliente> listarClientes() {
		return clienteService.obterClientes();
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarCliente(@RequestBody Cliente cliente) {
		clienteService.criarCliente(cliente);
	}
	
	@PatchMapping("/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deletarCliente(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}
	
}
